package com.xsisacademy.bootcamp.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;

import com.xsisacademy.bootcamp.model.Product;
import com.xsisacademy.bootcamp.model.Variant;

public interface VariantRepository extends JpaRepository<Variant, Long>{

	/*@Query(value = "UPDATE variant v SET is_active = false WHERE category_id = ?1", nativeQuery = true)
	@Transactional
	public void deleteVariantByCategoryId(Long id);*/
	
	@Modifying
	@Query(value = "UPDATE variant v SET is_active = false WHERE v.id = ?1", nativeQuery = true)
	@Transactional
	public void deleteVariantById(Long id);
	
	@Query(value = "SELECT * FROM variant v WHERE v.category_id = ?1", nativeQuery=true)
	public List<Variant> findByCategoryId(Long categoryId);
	
	@Query("FROM Variant WHERE LOWER(variantName) LIKE LOWER(CONCAT('%', ?1, '%')) ORDER BY variantName")
	public List<Variant> searchVariant(String keyword);
	
	public Page<Variant> findByIsActiveTrueOrderByVariantCode(Pageable pagingSort);
	
}
