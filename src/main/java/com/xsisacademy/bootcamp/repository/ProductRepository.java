package com.xsisacademy.bootcamp.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.xsisacademy.bootcamp.model.Category;
import com.xsisacademy.bootcamp.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {
	
	@Modifying
	@Query(value = "UPDATE product p SET is_active = false WHERE p.id = ?1", nativeQuery = true)
	@Transactional
	public void deleteProductById(Long id);
	
	@Query("FROM Product WHERE LOWER(productName) LIKE LOWER(CONCAT('%', ?1,'%')) ORDER BY productName")
	public List<Product> searchProduct(String keyword);
	
	public Page<Product> findByIsActiveTrueOrderByProductCode(Pageable pagingSort);
}