package com.xsisacademy.bootcamp.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.bootcamp.model.Category;

public interface CategoryRepository extends JpaRepository<Category /*model*/, Long> { //mengambil objek dr database
	@Query("FROM Category")
	public List<Category>findByCategory();
	
	@Modifying
	@Query(value = "UPDATE Category c SET is_active = false WHERE c.id = ?1", nativeQuery=true)
	@Transactional
	public void deleteCategoryById(Long id);
	
	@Query("FROM Category WHERE LOWER(categoryName) LIKE LOWER(CONCAT('%', ?1,'%')) ORDER BY categoryName")
	public List<Category> searchCategory(String keyword);

	public Page<Category> findByIsActiveTrueOrderByCategoryCode(Pageable pagingSort);
	
}
