package com.xsisacademy.bootcamp.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xsisacademy.bootcamp.model.Category;
import com.xsisacademy.bootcamp.model.Variant;
import com.xsisacademy.bootcamp.repository.CategoryRepository;
import com.xsisacademy.bootcamp.repository.VariantRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiVariantController {

	@Autowired
	public VariantRepository variantRepository;

//	@Autowired
//	public CategoryRepository categoryRepository;

	@GetMapping("variant")
	public ResponseEntity<List<Variant>> getAllVariant() {
		try {
//			List<Category> category = this.categoryRepository.findAll();
			List<Variant> variant = this.variantRepository.findAll();
			return new ResponseEntity<>(variant, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("variant/{id}")
	public ResponseEntity<List<Variant>> getVariantById(@PathVariable("id") Long id) {
		try {
			Optional<Variant> variant = this.variantRepository.findById(id);
			if (variant.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(variant, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<Variant>>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("variantbycategory/{catid}")
	public ResponseEntity<List<Variant>> getVariantByCategoryid(@PathVariable("catid") Long catid) {
		try {
			List<Variant> variantByCategoryId = this.variantRepository.findByCategoryId(catid);
			ResponseEntity rest = new ResponseEntity<>(variantByCategoryId, HttpStatus.OK);
			return rest;
		} catch (Exception e) {
			return new ResponseEntity<List<Variant>>(HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping("add/variant")
	public ResponseEntity<Object> saveVariant(@RequestBody Variant variant) {
		variant.setIsActive(true);
		Variant variantData = this.variantRepository.save(variant);
		if (variantData.equals(variant)) {
			return new ResponseEntity<Object>("Save Data Succesfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("edit/variant/{id}")
	public ResponseEntity<Object> updatedVariant(@RequestBody Variant variant, @PathVariable("id") Long id) {
		Optional<Variant> variantdata = this.variantRepository.findById(id);
		if (variantdata.isPresent()) {
			variant.setId(id);
			variant.setIsActive(true);
			this.variantRepository.save(variant);
			ResponseEntity rest = new ResponseEntity<>("Updated success", HttpStatus.OK);
			return rest;
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@PutMapping("delete/variant/{id}")
	public ResponseEntity<Object> deleteVariant(@PathVariable("id") Long id) {
		Optional<Variant> variantdata = this.variantRepository.findById(id);
		if (variantdata.isPresent()) {
			Variant variant = new Variant();
			variant.setId(id);
			this.variantRepository.deleteVariantById(id);
			ResponseEntity rest = new ResponseEntity<>("Deleted success", HttpStatus.OK);
			return rest;
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@PostMapping("variant/search")
	public ResponseEntity<List<Variant>> getVariantByName(@Param("keyword") String keyword) {
		if (!keyword.equals("")) {
			List<Variant> variant = this.variantRepository.searchVariant(keyword);
			return new ResponseEntity<List<Variant>>(variant, HttpStatus.OK);
		} else {
			List<Variant> variant = this.variantRepository.findAll();
			return new ResponseEntity<List<Variant>>(variant, HttpStatus.OK);
		}
	}

	@GetMapping("variant/paging")
	public ResponseEntity<Map<String, Object>> getAllVariants(@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "5") int size) {
		try {
			List<Variant> variant = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);

			Page<Variant> pageTuts;
			pageTuts = variantRepository.findByIsActiveTrueOrderByVariantCode(pagingSort);

			variant = pageTuts.getContent();

			Map<String, Object> response = new HashMap<>();
			response.put("variant", variant);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItems", pageTuts.getTotalElements());
			response.put("totalPages", pageTuts.getTotalPages());

			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
