package com.xsisacademy.bootcamp.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xsisacademy.bootcamp.model.Category;
import com.xsisacademy.bootcamp.model.Product;
import com.xsisacademy.bootcamp.model.Variant;
import com.xsisacademy.bootcamp.repository.ProductRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiProductController {

	@Autowired
	public ProductRepository productRepository;

	@GetMapping("product")
	public ResponseEntity<List<Product>> getAllProduct() {
		try {
			List<Product> product = this.productRepository.findAll();
			return new ResponseEntity<>(product, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping("product/{id}")
	public ResponseEntity<List<Product>> getProductById(@PathVariable("id") Long id) {
		try {
			Optional<Product> product = this.productRepository.findById(id);
			if (product.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(product, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<Product>>(HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping("add/product")
	public ResponseEntity<Object> saveProduct(@RequestBody Product product) {
		product.setIsActive(true);
		Product productData = this.productRepository.save(product);
		if (productData.equals(product)) {
			return new ResponseEntity<Object>("Save Data Succesfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("edit/product/{id}")
	public ResponseEntity<Object> updatedProduct(@RequestBody Product product, @PathVariable("id") Long id) {
		Optional<Product> productdata = this.productRepository.findById(id);
		if (productdata.isPresent()) {
			product.setId(id);
			product.setIsActive(true);
			this.productRepository.save(product);
			ResponseEntity rest = new ResponseEntity<>("Updated success", HttpStatus.OK);
			return rest;
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@PutMapping("delete/product/{id}")
	public ResponseEntity<Object> deleteProduct(@PathVariable("id") Long id) {
		Optional<Product> productdata = this.productRepository.findById(id);
		if (productdata.isPresent()) {
			Product product = new Product();
			product.setId(id);
			this.productRepository.deleteProductById(id);
			ResponseEntity rest = new ResponseEntity<>("Deleted success", HttpStatus.OK);
			return rest;
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@PostMapping("product/search")
	public ResponseEntity<List<Product>> getProductByName(@Param("keyword") String keyword) {
		if (!keyword.equals("")) {
			List<Product> product = this.productRepository.searchProduct(keyword);
			return new ResponseEntity<List<Product>>(product, HttpStatus.OK);
		} else {
			List<Product> product = this.productRepository.findAll();
			return new ResponseEntity<>(product, HttpStatus.OK);
		}
	}
	
	@GetMapping("product/paging")
	public ResponseEntity<Map<String, Object>> getAllProducts(@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "5") int size) {
		try {
			List<Product> product = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);
			
			Page<Product> pageTuts;
			pageTuts = productRepository.findByIsActiveTrueOrderByProductCode(pagingSort);
			
			product = pageTuts.getContent();
			
			Map<String, Object> response = new HashMap<>();
			response.put("product", product);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItems", pageTuts.getTotalElements());
			response.put("totalPages", pageTuts.getTotalPages());
			
			return new ResponseEntity<Map<String,Object>>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
