package com.xsisacademy.bootcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Xapos279Application {

	public static void main(String[] args) {
		SpringApplication.run(Xapos279Application.class, args);
	}

}
